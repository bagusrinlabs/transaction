<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class TransactionItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('transaction_items')->insert([
        	[	
        		'uuid' => Str::uuid(),
                'transaction_id' => 1,
            	'title' => 'espresso',
            	'qty' => 1,
                'price' => 15000
        	],
            [   
                'uuid' => Str::uuid(),
                'transaction_id' => 1,
                'title' => 'cafe latte',
                'qty' => 1,
                'price' => 25000
            ]
        ]);
    }
}
