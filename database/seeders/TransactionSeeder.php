<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class TransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('transactions')->insert([
        	[	
        		'uuid' => Str::uuid(),
                'user_id' => 2,
            	'total_amount' => 50000,
            	'paid_amount' => 100000,
                'change_amount' => 50000,
            	'payment_method' => 'cash'
        	]
        ]);
    }
}
