<?php
namespace App\Http\Middleware;

use Closure;
use Exception;
use App\Models\User;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;

use App\Core\Manager\RedisManager;

class JwtMiddleware
{
    public function handle($request, Closure $next, $guard = null)
    {
        $token = $request->get('token');

        if(!$token) {
            return response()->json([
                'success' => false,
                'code' => 'inv',
                'message' => 'Invalid token'
            ], 401);
        }

        try {

            $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
        
        } catch(ExpiredException $e) {
            
            return response()->json([
                'success' => false,
                'code' => 'exp',
                'message' => 'Token is expired.'
            ], 400);
        
        } catch(Exception $e) {
            return response()->json([
                'success' => false,
                'code' => 'inv',
                'error' => 'Invalid token.'
            ], 401);
        }


        //Get data token from redis
        $redis = new RedisManager();
        $key = $token;
        $data = $redis->get($key, []);

        $request->request->add(['user' => $data]);
        
        return $next($request);
    }
}