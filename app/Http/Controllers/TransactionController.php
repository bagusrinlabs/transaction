<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\Core\Manager\RedisManager;
use App\Models\Transaction;
use App\Models\TransactionItems;
use Validator;

class TransactionController extends Controller
{   


    private $request;
    
    public function __construct(Request $request) {
        $this->request = $request;
    }

    public function list(){

        try {

            $transactions = Transaction::with('transaction_items')->paginate(10);

        	return response()->json([
                    'success' => true,
                    'data' => $transactions
                ], 200);

        } catch (\Exception $e) {

            return response()->json(['success' => false, 'message' => 'error'], 500);

        }
    }


    public function store(){   

        DB::beginTransaction();

        try {

            $items = json_decode($this->request->getContent(), true);
            $total_price = 0;

            foreach($items["items"] as $val){
                $total_price += $val['qty']*$val['price']; 
            }

            DB::table('transactions')->insert([
                'uuid' => Str::uuid(),
                'user_id' => $this->request->get('user')['id'],
                'total_amount' => $total_price,
                'paid_amount' => 0,
                'change_amount' => 0,
                'payment_method' => $items['payment_method']
            ]);

            $transaction_id = DB::getPdo()->lastInsertId();

            $data_items = [];
            foreach($items["items"] as $val){

                $total_price += $val['qty']*$val['price']; 

                $data_items[] = [
                    "uuid" => Str::uuid(),
                    "transaction_id" => $transaction_id,
                    "title" => $val["title"],
                    "qty" => $val["qty"],
                    "price" => $val["price"]
                ];

            }

            DB::table('transaction_items')->insert($data_items);

            $ret = DB::commit();

            return response()->json(['success' => true, 'message' => 'successfully inserted'], 200);

        } catch (\Exception $e) {

            DB::rollback();
            return response()->json(['success' => false, 'message' => 'failed to insert'], 500);

        }
    }

    public function update($id){

        try {

            $items = json_decode($this->request->getContent(), true);

            $transaction = Transaction::find($id);
            $transaction->payment_method = $items['payment_method'];
            $transaction->paid_amount = $items['paid_amount'];
            $transaction->change_amount = $items['change_amount'];
            $transaction->updated_at = now();
            $transaction->save();

            return response()->json([
                    'success' => true,
                    'message' => 'successfully updated'
                ], 200);

        } catch (\Exception $e) {

            return response()->json(['success' => false, 'message' => 'error'], 500);

        }

    }

    public function delete($id){

        DB::beginTransaction();

        try {

            $transaction = Transaction::find($id);
            $transaction->delete();

            $transaction_items = TransactionItems::where('transaction_id', $id);
            $transaction_items->delete();

            DB::commit();

            return response()->json([
                    'success' => true,
                    'message' => 'successfully deleted'
                ], 200);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['success' => false, 'message' => 'error'], 500);

        }

    }
}
