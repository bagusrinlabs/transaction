<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransactionItems extends Model
{
    use HasFactory, SoftDeletes;

    protected $connection = 'mysql';
    protected $table = 'transaction_items';

    protected $fillable = [
        'id',
        'uuid',
        'transaction_id',
        'title',
        'qty',
        'price',
        'created_at',
        'updated_at'
    ];

    protected $hidden = ['deleted_at'];
    protected $dates = ['deleted_at'];

    public function transactions(){
        return $this->belongsTo(Transaction::class,'transaction_id');
    }
}
