<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use HasFactory, SoftDeletes;

    protected $connection = 'mysql';
    protected $table = 'transactions';

    protected $fillable = [
        'id',
        'uuid',
        'user_id',
        'device_timestamp',
        'total_amount',
        'paid_amount',
        'change_amount',
        'payment_amount',
        'created_at',
        'updated_at'
    ];

    protected $hidden = ['deleted_at'];
    protected $dates = ['deleted_at'];

    public function transaction_items(){
       return $this->hasMany(TransactionItems::class);
    }
}
